import { useContext } from 'react';
import AppContext from '../state/app/context';

export default () => useContext(AppContext);
