import useApi from '../useApi';
import config from '../../config';

export default () => {
    const [data, callApi] = useApi();

    const loginRequest = (body) => {
        callApi(`${config.API_URL}/auth/authenticate`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });
    };

    return [data, loginRequest];
};
