import { useState } from 'react';

export default () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [result, setResult] = useState(null);

    const request = async (url, options) => {
        setLoading(true);
        setError(null);
        try {
            const response = await fetch(url, options);
            // Treat non-2xx series responses as errors.
            if (!response.ok) {
                throw new Error(`Request to [${url}] with ${JSON.stringify(options)} failed with: ${response.status} - ${response.statusText}`);
            }
            setResult(response);
        } catch (e) {
            setError(e.message);
        }
        setLoading(false);
    };

    return [{loading, error, result}, request];
};
