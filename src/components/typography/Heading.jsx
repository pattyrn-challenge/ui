import styled from 'styled-components';

export default styled.h6`
    color: #1C1C34;
    font-family: 'Poppins', sans-serif;
    font-style: normal;
    font-weight: 600;
    margin: 0;
`;
