import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Heading from './Heading';

const TitleStyle = styled(Heading)`
    font-size: 3rem;
    line-height: 4.5rem;
    margin-top: 1.8rem;
`;

const Title = ({ children }) => (<TitleStyle as="h3">{children}</TitleStyle>);

Title.propTypes = {
    children: PropTypes.node
};

export default Title;
