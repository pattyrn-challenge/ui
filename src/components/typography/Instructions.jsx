import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Heading from './Heading';

const InstructionsStyle = styled(Heading)`
    font-size: 1.6rem;
    line-height: 1.8rem;
    opacity: 0.6;
    margin-top: 2rem;
`;

const Instructions = ({ children }) => (<InstructionsStyle as="h6">{children}</InstructionsStyle>);

Instructions.propTypes = {
    children: PropTypes.node
};

export default Instructions;
