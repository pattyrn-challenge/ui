import React from 'react';
import { render } from '@testing-library/react';
import Title from '../Title';

describe('Title', () => {
    it('should match snapshot', () => {
        const { container } = render(<Title />);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when title contains child text', () => {
        const { container } = render(<Title>This is the contents</Title>);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when title contains child elements', () => {
        const { container } = render(<Title><div>content</div><p>Text</p></Title>);
        expect(container).toMatchSnapshot();
    });
});
