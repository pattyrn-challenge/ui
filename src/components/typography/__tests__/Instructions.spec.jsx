import React from 'react';
import { render } from '@testing-library/react';
import Instructions from '../Instructions';

describe('Instructions', () => {
    it('should match snapshot', () => {
        const { container } = render(<Instructions />);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when instructions contains child text', () => {
        const { container } = render(<Instructions>This is the contents</Instructions>);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when instructions contains child elements', () => {
        const { container } = render(<Instructions><div>content</div><p>Text</p></Instructions>);
        expect(container).toMatchSnapshot();
    });
});
