import React from 'react';
import { render } from '@testing-library/react';
import Heading from '../Heading';

describe('Heading', () => {
    it('should match snapshot', () => {
        const { container } = render(<Heading />);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when heading contains child text', () => {
        const { container } = render(<Heading>This is the contents</Heading>);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when heading contains child elements', () => {
        const { container } = render(<Heading><div>content</div><p>Text</p></Heading>);
        expect(container).toMatchSnapshot();
    });
});
