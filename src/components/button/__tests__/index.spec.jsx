import React from 'react';
import { render } from '@testing-library/react';
import Button from '../index';

describe('Button', () => {
    it.each([true, false])('should match snapshot when disabled is %s', disabled => {
        const { container } = render(
            <Button disabled={disabled}>Button Text</Button>
        );
        expect(container).toMatchSnapshot();
    });
});
