import styled from 'styled-components';

export default styled.button`
    background-color: ${({ disabled }) => disabled ? 'rgba(196, 196, 196, 0.5)' : '#F29F9B'};
    border-radius: 0.25rem;
    border: 0;
    cursor: ${({ disabled }) => disabled ? 'not-allowed' : 'pointer'};
    color: ${({ disabled }) => disabled ? '#919193' : '#1C1C34'};
    font-family: 'Open Sans', sans-serif;
    font-size: 0.9rem;
    height: 3.8rem;
    line-height: 1.2rem;
`;
