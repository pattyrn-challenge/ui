import { Form as FormikForm } from 'formik';
import styled from 'styled-components';

export default styled(FormikForm)`
    border-radius: 1.4rem;
    box-shadow: 0 0 1.5rem rgba(0, 0, 0, 0.12);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-top: 3.8rem;
    padding: 3rem;
`;
