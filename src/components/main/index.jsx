import React from 'react';
import Heading from '../typography/Heading';

export default () => (<Heading as="h3">you made it!</Heading>);
