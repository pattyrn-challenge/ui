import React from 'react';
import { render } from '@testing-library/react';
import App from '../index';

jest.mock('../../router', () => () => (<div id="Router" />));

describe('App', () => {
  it('should match snapshot', () => {
    const { container } = render(<App />);
    expect(container).toMatchSnapshot();
  });
});
