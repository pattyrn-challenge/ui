import React, { useState } from 'react';
import Router from '../router';
import styled from 'styled-components';
import AppContext from '../../state/app/context';

const App = styled.div`
	margin: auto;
	max-width: 35rem;
	font-family: 'Open Sans', sans-serif;
	font-style: normal;
`;

export default () => {
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	return (
		<AppContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
			<App>
				<Router />
			</App>
		</AppContext.Provider>
	);
};
