import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import Form from '../form';
import Field from '../field';
import styled from 'styled-components';
import loginSchema from './loginSchema';
import Button from '../button';

const Agree = styled.p`
    margin: auto;
    margin-top: 0.8rem;
`;

const SubmitButton = styled(Button)`
    margin-top: 3rem;
`;

const LoginForm = ({ loginRequest }) => (
    <Formik
        initialValues={{
            email: '',
            password: ''
        }}
        validationSchema={loginSchema}
        onSubmit={values => loginRequest(values)}
    >
        {({ dirty, isValid }) => {
            const disableSubmit = !(dirty && isValid);
            return (
                <Form>
                    <Field label="Email" type="email" placeholder="email@example.com" name="email" />
                    <Field label="Password" type="password" name="password" />
                    <SubmitButton type="submit" disabled={disableSubmit}>NEXT</SubmitButton>
                    <Agree>By clicking "next" you agree to be awesome</Agree>
                </Form>
            );
        }}
    </Formik>
);

LoginForm.propTypes = {
    loginRequest: PropTypes.func.isRequired
};

export default LoginForm;
