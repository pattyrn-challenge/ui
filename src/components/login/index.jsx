import React, { useEffect } from 'react';
import useAppContext from '../../hooks/useAppContext';
import { navigate } from '@reach/router';
import Title from '../typography/Title';
import Instructions from '../typography/Instructions';
import LoginForm from './LoginForm';
import useLoginApi from '../../hooks/login/useLoginApi';

export default () => {
    const { setIsLoggedIn } = useAppContext();
    const [{ result, loading, error }, loginRequest] = useLoginApi();

    useEffect(() => {
        // TODO Check more of the result returned to verify successful login
        if (!loading && !error && result) {
            setIsLoggedIn(true);
            navigate('/');
        } else {
            setIsLoggedIn(false);
        }
    }, [result, loading, error, setIsLoggedIn]);

    return (
        <div>
            <Title>Howzit?!</Title>
            <Instructions>To get started, please fill out the form below to validate your account.</Instructions>
            <LoginForm loginRequest={loginRequest} />
        </div>
    );
};
