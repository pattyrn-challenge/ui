import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field as FormikField } from 'formik';
import Field from './Field';
import PasswordField from './PasswordField';
import styled from 'styled-components';

const StyledFieldContainer = styled.div`
    border-radius: 0.6rem;
    border: 0.2rem solid rgba(28, 28, 52, 0.3);
    border-color: ${({ focused }) => focused && "#F29F9B"};
    box-sizing: border-box;
    display: flex;
    height: 4.5rem;
    line-height: 1.5rem;
    position: relative;
    transition: all 0.2s;
    
    &:not(:first-of-type) {
        margin-top: 1.8rem;
    }
`;

const FieldContainer = props => {
    const [focused, setIsFocused] = useState(false);
    const {
        name,
        placeholder,
        label,
        type
    } = props;
    const childProps = {
        id: name,
        placeholder: placeholder || label,
        onFocus: () => setIsFocused(true),
        onBlur: () => setIsFocused(false),
        ...props
    };
    return (
        <StyledFieldContainer focused={focused}>
            {type === "password" ? (<PasswordField {...childProps} />) : (<Field {...childProps} />)}
        </StyledFieldContainer>
    );
};

FieldContainer.propTypes = {
    ...FormikField.propTypes,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired
}

export default FieldContainer;
