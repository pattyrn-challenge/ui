import React from 'react';
import PropTypes from 'prop-types';
import { Field as FormikField } from 'formik';
import styled from 'styled-components';

const StyledField = styled(FormikField)`
    border-radius: 0.6rem;
    border: 0;
    box-sizing: border-box;
    color: #1C1C34;
    font-family: 'Poppins', sans-serif;
    font-size: 1rem;
    height: 100%;
    line-height: 1.5rem;
    outline: 0;
    padding: 0 0 0 2rem;
    width: 100%;

    &::placeholder {
        opacity: 1;
        transition: all 0.2s;
    }

    /* Don't show the placeholder when not focused, the label will cover it */
    &:placeholder-shown:not(:focus)::placeholder {
        opacity: 0;
    }
`;

const StyledLabel = styled.label`
    background-color: white;
    font-size: 0.8rem;
    left: 1.75rem;
    padding: 0 0.25rem;
    position: absolute;
    top: -0.9rem;
    transition: all 0.2s;

    /* when focused on the input change the color of the label to match the outline */
    ${StyledField}:focus + & {
        color: #F29F9B;
    }

    /* Move the label down to take the place of the input placeholder */
    ${StyledField}:placeholder-shown:not(:focus) + & {
        cursor: text;
        font-size: 1rem;
        opacity: 0.5;
        top: 32%;
    }
`;

const Field = props => {
    const {
        name,
        label
    } = props;

    return (
        <>
            <StyledField
                id={name}
                {...props}
            />
            <StyledLabel htmlFor={name}>{label}</StyledLabel>
        </>
    );
};

Field.propTypes = {
    ...FormikField.propTypes,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
}

export default Field;
