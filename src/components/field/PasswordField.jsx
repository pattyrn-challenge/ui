import React, { useEffect, useState, useRef } from 'react';
import { Field as FormikField } from 'formik';
import styled from 'styled-components';
import Field from './Field';
import Eye from '../../icons/eye.svg';

const PasswordVisibility = styled.img`
    cursor: pointer;
    padding-right: 1.6rem;
`;

const PasswordField = props => {
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [inputType, setInputType] = useState('password');
    const inputRef = useRef();

    useEffect(() => {
        setInputType(isPasswordVisible ? 'text' : 'password');
    }, [isPasswordVisible]);

    return (
        <>
            <Field
                {...props}
                type={inputType}
                innerRef={inputRef}
            />
            <PasswordVisibility
                src={Eye}
                onClick={() => {
                    setIsPasswordVisible(!isPasswordVisible);
                    inputRef.current.focus();
                }}
                alt="Toggle password visibility"
            />
        </>
    );
};

PasswordField.propTypes = {
    ...FormikField.propTypes
}

export default PasswordField;
