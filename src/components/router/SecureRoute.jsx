import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from '@reach/router';
import useAppContext from '../../hooks/useAppContext';

const SecureRoute = ({ children }) => {
    const { isLoggedIn } = useAppContext();
    return isLoggedIn ? children : <Redirect to="/login" noThrow />
};

SecureRoute.propTypes = {
    children: PropTypes.node
};

export default SecureRoute;
