import React from 'react';
import { render } from '@testing-library/react';
import SecureRoute from '../SecureRoute';
import AppContext from '../../../state/app/context';

describe('SecureRoute', () => {
    const SecureChild = () => (<div id="secure-child" />);

    it.each([true, false])('should match snapshot when isLoggedIn is %s', isLoggedIn => {
        const { container } = render(
            <AppContext.Provider value={{ isLoggedIn }}>
                <SecureRoute>
                    <SecureChild />
                </SecureRoute>
            </AppContext.Provider>
        );
        expect(container).toMatchSnapshot();
    });
});
