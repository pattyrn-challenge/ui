import React from 'react';
import { Router } from '@reach/router';
import SecureRoute from './SecureRoute';
import Login from '../login';
import Main from '../main';

export default () => (
    <Router>
        <Login path="login" />
        <SecureRoute default>
            <Main default />
        </SecureRoute>
    </Router>
);
